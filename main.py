from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

service = Service('C:\\Users\\veree\\chromedriver\\chromedriver_win32\\chromedriver.exe')

driver = webdriver.Chrome(service=service)

driver.get(
    'https://www.amazon.com/Instant-Pot-Plus-60-Programmable/dp/B01NBKTPTS/ref=sr_1_1?crid=25ZF01VADVYS7&keywords=inst'
    'ant%2Bpot%2Bduo%2Bevo%2Bplus%2B9-in-1&qid=1644150385&sprefix=instant%2Bpot%2Bduo%2Bevo%2Bplus%2B9%2Caps%2C305&sr='
    '8-1&th=1m')

price = driver.find_element(By.XPATH, "//td[@class='a-span12']")
print(price.tag_name)
print(price.text)

product_name = driver.find_element(by=By.ID, value='productTitle')
print(product_name.text)

by_CSS_selector = driver.find_element(by=By.CSS_SELECTOR, value='#title #productTitle')
print(by_CSS_selector.text)

by_XPATH = driver.find_element(by=By.XPATH, value='//*[@id="poExpander"]/div[1]/div/table/tbody/tr[1]/td[2]/span')

print(by_XPATH.text)

all_by_CSS_selector = driver.find_elements(by=By.CSS_SELECTOR, value='#title')
for i in all_by_CSS_selector:
    print(i.text)

driver.quit()
