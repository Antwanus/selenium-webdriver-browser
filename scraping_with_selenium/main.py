from pprint import pprint

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

service = Service('C:\\Users\\veree\\chromedriver\\chromedriver_win32\\chromedriver.exe')
driver = webdriver.Chrome(service=service)
driver.get('https://www.python.org/')

object_list = driver.find_elements(By.XPATH,
                                   '// *[ @ id = "content"] / div / section / div[2] / div[2] / div / ul / li')
events_list = [e.text for e in object_list]

result_dictionary = {}
for i in range(len(events_list)):
    result_dictionary[i] = {'time': events_list[i].split('\n')[0], 'name': events_list[i].split('\n')[1]}

pprint(result_dictionary)

driver.quit()
