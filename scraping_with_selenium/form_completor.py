from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

service = Service('C:\\Users\\veree\\chromedriver\\chromedriver_win32\\chromedriver.exe')
driver = webdriver.Chrome(service=service)
driver.get('https://secure-retreat-92358.herokuapp.com/')

first_name_input = driver.find_element(By.XPATH, '/html/body/form/input[1]')
first_name_input.send_keys('bill')
first_name_input.send_keys(Keys.TAB)
last_name_input = driver.find_element(By.XPATH, '/html/body/form/input[2]')
last_name_input.send_keys('gates')
last_name_input.send_keys(Keys.TAB)
email_input = driver.find_element(By.XPATH, '/html/body/form/input[3]')
email_input.send_keys('billgates@microsoft.com')
email_input.send_keys(Keys.TAB)
email_input.send_keys(Keys.ENTER)

driver.quit()
