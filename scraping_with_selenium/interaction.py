from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

service = Service('C:\\Users\\veree\\chromedriver\\chromedriver_win32\\chromedriver.exe')
driver = webdriver.Chrome(service=service)
driver.get('https://en.wikipedia.org/wiki/Main_Page')

article_count = driver.find_element(By.XPATH, '//*[@id="articlecount"]/a[1]')
# article_count.click()
print(article_count.text)

all_portals_anchor = driver.find_element(By.LINK_TEXT, 'All portals')
all_portals_anchor.click()

input_box = driver.find_element(By.XPATH, '//*[@id="searchInput"]')
input_box.send_keys('python')
input_box.send_keys(Keys.ENTER)

# driver.quit()
